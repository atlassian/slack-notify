import json
import os
import pathlib

import requests
import yaml

from bitbucket_pipes_toolkit import Pipe


# global variables
REQUESTS_DEFAULT_TIMEOUT = 10
BASE_SUCCESS_MESSAGE = "Notification successful"
BASE_FAILED_MESSAGE = "Notification failed"


# defines the schema for pipe variables
schema = {
    "WEBHOOK_URL": {
        "type": "string",
        "required": True
    },
    "MESSAGE": {
        "type": "string",
        "required": True,
        "excludes": "PAYLOAD_FILE"
    },
    "PRETEXT": {
        "type": "string",
        "required": False,
        "excludes": "PAYLOAD_FILE"
    },
    "PAYLOAD_FILE": {
        "type": "string",
        "required": True,
        "excludes": ["MESSAGE", "PRETEXT"]
    },
    "DEBUG": {
        "type": "boolean",
        "required": False,
        "default": False
    }
}


class SlackNotifyPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.webhook_url = self.get_variable("WEBHOOK_URL")
        self.message = self.get_variable("MESSAGE")
        self.pretext = self.get_variable("PRETEXT")
        self.payload_file_path = self.get_variable("PAYLOAD_FILE")
        self.debug = self.get_variable("DEBUG")

    def run(self):
        if self.payload_file_path:
            self.log_info("Starting with payload provided in PAYLOAD_FILE...")

            if not pathlib.Path(self.payload_file_path).exists():
                self.fail("Passed PAYLOAD_FILE path does not exist.")

        self.log_info("Sending notification to Slack...")

        headers = {'Content-Type': 'application/json'}

        if self.pretext is None:
            self.pretext = f"Notification sent from <https://bitbucket.org/" \
                f"{os.environ['BITBUCKET_WORKSPACE']}/{os.environ['BITBUCKET_REPO_SLUG']}/" \
                f"addon/pipelines/home#!/results/{os.environ['BITBUCKET_BUILD_NUMBER']}|" \
                f"Pipeline #{os.environ['BITBUCKET_BUILD_NUMBER']}>"

        if self.payload_file_path:
            try:
                with open(self.payload_file_path, 'r') as payload_file:
                    payload = json.loads(payload_file.read())
            except json.JSONDecodeError:
                self.fail(f'Failed to parse PAYLOAD_FILE {self.payload_file_path}: invalid JSON provided.')
        else:
            payload = {
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": self.pretext
                        }
                    },
                    {
                        "type": "divider"
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": self.message
                        }
                    }
                ]
            }

        try:
            response = requests.post(
                url=self.webhook_url,
                headers=headers,
                json=payload,
                timeout=REQUESTS_DEFAULT_TIMEOUT
            )
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            self.fail(f'{BASE_FAILED_MESSAGE}. Pipe has finished with an error: {e}')

        self.log_info(f"HTTP Response: {response.text}")

        self.success(BASE_SUCCESS_MESSAGE)


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = SlackNotifyPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
